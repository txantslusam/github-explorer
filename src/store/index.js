import Vue from 'vue';
import Vuex from 'vuex';
import _users from './_users';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    users: _users,
  },
});
