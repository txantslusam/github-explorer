import Axios from 'axios';

export default {
  namespaced: true,
  state: {
    userNames: [],
    users: {},
    activeUser: {},
  },

  mutations: {
    setUserNames(state, userNames) {
      state.userNames = userNames;
    },

    setActiveUser(state, user) {
      state.activeUser = user;
    },

    saveUser(state, savedUserObject) {
      state.users = { ...state.users, ...savedUserObject };
    },
  },

  actions: {
    async getUserDetails({ commit, state, dispatch }, userName) {
      const savedUser = state.users[userName];
      if (savedUser) {
        commit('setActiveUser', savedUser);
        return;
      }
      const response = await Axios.get(`https://api.github.com/users/${userName}`);
      const user = response.data;
      commit('setActiveUser', user);
      commit('saveUser', { [user.login.toLowerCase()]: user });
      await dispatch('getUserRepositories', {
        reposUrl: user.repos_url,
        params: { page: 1 },
      });
    },

    async getUserRepositories({ state, commit }, { reposUrl, params }) {
      const response = await Axios.get(reposUrl, { params });
      const repos = response.data;
      const allRepos = state.activeUser.repos ? [...state.activeUser.repos, ...repos] : repos;
      const user = { ...state.activeUser, repos: allRepos };
      commit('setActiveUser', user);
      commit('saveUser', { [user.login.toLowerCase()]: user });
    },
  },
};
